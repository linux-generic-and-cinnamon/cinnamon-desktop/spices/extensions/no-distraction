const Settings = imports.ui.settings;
const Tweener = imports.ui.tweener;
const SignalManager = imports.misc.signalManager;

let settings;
let sigMgr;

function init(metadata)
{
	sigMgr = new SignalManager.SignalManager(null);
	sigMgr.connect(global.display, "notify::focus-window", onFocusChange);
	settings = new SettingsHandler(metadata.uuid);
//	myname = metadata.name+" "+metadata.version;
}

function SettingsHandler(uuid) {
	this._init(uuid);
}

SettingsHandler.prototype = {
	_init: function(uuid) {
	this.settings = new Settings.ExtensionSettings(this, uuid);
	this.settings.bindProperty(Settings.BindingDirection.IN, "opacity", "opacity", function(){});
	this.settings.bindProperty(Settings.BindingDirection.IN, "inTime", "inTime", function(){});
	this.settings.bindProperty(Settings.BindingDirection.IN, "inEffect", "inEffect", function(){});
	this.settings.bindProperty(Settings.BindingDirection.IN, "outTime", "outTime", function(){});
	this.settings.bindProperty(Settings.BindingDirection.IN, "outEffect", "outEffect", function(){});
	}
}

function onFocusChange() {
	let focusWindow = global.display.get_focus_window();
	if (focusWindow) {
		let focusActor = focusWindow.get_compositor_private();
		if (!focusActor) focusWindow.reset_opacity();
		else Tweener.addTween(focusActor, {
			opacity: 255,
			time: settings.inTime/1000,
			transition: settings.inEffect
		});
	}
	let windowList = global.display.list_windows(0);
	for (let wnd of windowList) {
		if (global.display.xwindow_is_a_no_focus_window (wnd)
		|| focusWindow == wnd) continue;
		let actor = wnd.get_compositor_private();
		if (!actor) continue;
		Tweener.addTween(actor, {
			opacity: settings.opacity,
			time: settings.outTime/1000,
			transition: settings.outEffect
		});
	}
}

function restoreAll() {
	let windowList = global.display.list_windows(0);
	for (let wnd of windowList) {
		if (global.display.xwindow_is_a_no_focus_window (wnd)) continue;
		let actor = wnd.get_compositor_private();
		if (!actor) wnd.reset_opacity();
		else Tweener.addTween(actor, {
			opacity: 255,
			time: settings.inTime/1000,
			transition: settings.inEffect
		});
	}
}

function enable()
{
	try { onFocusChange(); }
	catch(e) { global.log(e); }
}

function disable()
{
	try { sigMgr.disconnectAllSignals(); }
	catch(e) { global.log(e); }
	restoreAll();
}
